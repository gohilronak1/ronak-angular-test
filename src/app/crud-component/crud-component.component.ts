import { Component, OnInit } from '@angular/core';
import { Product } from './product.model';

@Component({
  selector: 'app-crud-component',
  templateUrl: './crud-component.component.html',
  styleUrls: ['./crud-component.component.css']
})
export class CrudComponentComponent {

  products: Product[] = [
    { id: 1, name: 'Product 1', price: 22.5 },
    { id: 2, name: 'Product 2', price: 23.5 }
  ];

  productMaxId = 2;

  productFormValue: Product;

  shouldShowModal = false;

  constructor() { }

  newProduct() {
    this.productFormValue = new Product();
    this.showModal();
  }

  editProduct(product: Product) {
    this.productFormValue = product;
    this.showModal();
  }

  deleteProduct(index: number) {
    this.products.splice(index, 1);
  }

  submit() {
    if (!this.productFormValue.id) {
      this.productMaxId++;
      this.productFormValue.id = this.productMaxId;
      this.products.push(this.productFormValue);
    }
    this.closeModal();
  }

  showModal() { this.shouldShowModal = true; }

  closeModal() { this.shouldShowModal = false; }

}
